# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.views.generic import View
from django.shortcuts import render
from django.http import QueryDict
from django.http import HttpResponse
from django.core.exceptions import ObjectDoesNotExist
from django.utils.datastructures import MultiValueDictKeyError
from django.db import IntegrityError, DataError, connection
from .models import Player, Game, GameInfo
from .decorators import check_exceptions


class Register(View):
    @check_exceptions
    def post(self, request):
        if len(Player.objects.filter(id = 1)) == 0:
            initobj = Player(id = 1, name = 'Not Decided')
            initobj.save()
        try:
            the_name = request.POST['name']
            pobj = Player(name = the_name)
            pobj.save()
            response = 'Your Player id is: ' + str(pobj.id) + ', Total DB calls = '+ str(len(connection.queries))
            return HttpResponse(response)
        except IntegrityError:
            return HttpResponse('Error adding entry: please resolve conflict',
            status = 500)
        except MultiValueDictKeyError:
            return HttpResponse('Error adding entry: MultiValueDictKeyError',
            status = 500)
        except DataError:
            return HttpResponse('Error adding entry: Value too long',
            status = 500)

class NewGame(View):
    @check_exceptions
    def post(self, request):
        input = request.POST
        white = input['white']
        black = input['black']

        Whiteobj = Player.objects.get(name = white)
        Blackobj = Player.objects.get(name = black)
        DefaultWin = Player.objects.get(id = 1)
        Gameobj = Game(player_white = Whiteobj, player_black = Blackobj,
        winner = DefaultWin)
        Gameobj.save()
        response = 'Your Game id is: ' + str(Gameobj.id) + ', Total DB calls =' + str(len(connection.queries))

        return HttpResponse(response, status = 200)

class Play(View):
    row = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h']
    col = ['1', '2', '3', '4', '5', '6', '7', '8']
    @check_exceptions
    def white_checker(self, row_index, col_index):
        answer = ['none', 'none']
        if row_index == 0 and col_index < 7:
            answer[0] = self.row[row_index + 1] + self.col[col_index + 1]
        elif row_index == 7 and col_index < 7:
            answer[1] = self.row[row_index - 1] + self.col[col_index + 1]
        elif col_index < 7:
            answer[0] = self.row[row_index + 1] + self.col[col_index + 1]
            answer[1] = self.row[row_index - 1] + self.col[col_index + 1]
        return answer

    @check_exceptions
    def black_checker(self, row_index, col_index):
        answer = ['none', 'none']
        if row_index == 0 and col_index > 1:
            answer[0] = self.row[row_index + 1] + self.col[col_index - 1]
        elif row_index == 7 and col_index > 1:
            answer[1] = self.row[row_index - 1] + self.col[col_index - 1]
        elif col_index > 1:
            answer[0] = self.row[row_index + 1] + self.col[col_index - 1]
            answer[1] = self.row[row_index - 1] + self.col[col_index - 1]
        return answer

    @check_exceptions
    def post(self, request):
        input_id = request.POST['game_id']
        white_play = request.POST['white_move']
        black_play = request.POST['black_move']
        true_white = white_play
        true_black = black_play
        game_obj = Game.objects.get(id = input_id)
        obj1 = GameInfo.objects.filter(game_id = input_id)
        max = len(obj1) - 1
        if max > -1:
            white_count = obj1[max].white_pawn_count
            black_count = obj1[max].black_pawn_count
        else:
            white_count = 15
            black_count = 15

        white_king = ''
        black_king = ''
        if 'x' in white_play:
            black_count = black_count - 1
            true_white = white_play.split('x')[1]

        if 'x' in black_play:
            white_count = white_count - 1
            true_black = black_play.split('x')[1]

        if 'END' in white_play:
            pobj = Player.objects.get(id = game_obj.player_white.id)
            game_obj.winner = pobj
            game_obj.save()

            game_info_obj = GameInfo(game_id = game_obj,
            white_move = white_play, black_move = black_play,
            white_pawn_count = white_count, black_pawn_count = black_count,
            white_king_pos = str(obj1[max].white_king_pos),
            black_king_pos = str(obj1[max].black_king_pos))

            game_info_obj.save()
            return HttpResponse('White wins', status = 200)
        elif 'END' in black_play:
            pobj = Player.objects.get(id = game_obj.player_black.id)
            game_obj.winner = pobj
            game_obj.save()

            game_info_obj = GameInfo(game_id = game_obj,
            white_move = white_play, black_move = black_play,
            white_pawn_count = white_count, black_pawn_count = black_count,
            white_king_pos = str(obj1[max].white_king_pos),
            black_king_pos = str(obj1[max].black_king_pos))

            game_info_obj.save()
            return HttpResponse('Black wins', status = 200)
        if 'K' in white_play or 'K' in black_play:
            # moves = the_move.split('-')
            if 'Kx' in white_play:
                white_king = white_king + white_play.split('Kx')[1]
                true_white = white_king
            elif 'K' in white_play:
                white_king = white_king + white_play.split('K')[1]
                true_white = white_king
            else:
                white_king = white_king + str(obj1[max].white_king_pos)

            if 'Kx' in black_play:
                black_king = black_king + black_play.split('Kx')[1]
                true_black = black_king
            elif 'K' in black_play:
                black_king = black_king + black_play.split('K')[1]
                true_black = black_king
            else:
                black_king = black_king + str(obj1[max].black_king_pos)

            game_info_obj = GameInfo(game_id = game_obj,
            white_move = white_play, black_move = black_play,
            white_pawn_count = white_count, black_pawn_count = black_count,
            white_king_pos = white_king, black_king_pos = black_king)

            game_info_obj.save()
        else:
            if max > -1:
                game_info_obj = GameInfo(game_id = game_obj,
                white_move = white_play, black_move = black_play,
                white_pawn_count = white_count, black_pawn_count = black_count,
                white_king_pos = str(obj1[max].white_king_pos),
                black_king_pos = str(obj1[max].black_king_pos))

                game_info_obj.save()
            else:
                game_info_obj = GameInfo(game_id = game_obj,
                white_move = white_play, black_move = black_play)
                game_info_obj.save()
                response = 'First Move played, ' + 'Total DB calls = ' + str(len(connection.queries))
                return HttpResponse(response, status = 200)



        white_check_value = ''
        black_check_value = ''

        black_king = str(obj1[max].black_king_pos)
        row_index = self.row.index(black_king[0])
        col_index = self.col.index(black_king[1])
        black_check = self.black_checker(row_index, col_index)

        if true_white in black_check:
            black_check_value = 'B'


        white_king = str(obj1[max].white_king_pos)
        row_index = self.row.index(white_king[0])
        col_index = self.col.index(white_king[1])
        white_check = self.white_checker(row_index, col_index)

        if true_black in white_check:
            white_check_value = 'W'

        check_value = white_check_value + black_check_value
        if len(check_value):
            game_info_obj.check = check_value
            game_info_obj.save()

        response = 'Move played, ' + 'Total DB calls = ' + str(len(connection.queries))
        return HttpResponse(response, status = 200)

    @check_exceptions
    def get(self, request):
        # return HttpResponse(request.GET.get('player_id'))
        if request.GET.get('player_id'):
            p_id = request.GET.get('player_id')
            black_obj = Game.objects.filter(player_black_id = p_id).order_by('id')
            white_obj = Game.objects.filter(player_white_id = p_id).order_by('id')

            if not len(black_obj) and not len(white_obj):
                raise ObjectDoesNotExist

            html = []

            htm_format = '<tr><td>%s</td><td>%s</td><td>%s</td><td>%s</td></tr>'

            html.append(htm_format %('Game id', 'Player Black', 'Player White', 'Winner'))
            for tuple in black_obj:
                html.append(htm_format %(tuple.id,tuple.player_black.name,tuple.player_white.name,tuple.winner.name))
            for tuple in white_obj:
                html.append(htm_format %(tuple.id,tuple.player_black.name,tuple.player_white.name,tuple.winner.name))

            html.append('Total db calls = %s'% str(len(connection.queries)))

            return HttpResponse('<table>%s</table>' % '\n'.join(html),status=200)
        elif request.GET.get('game_id'):
            g_id = request.GET.get('game_id')
            game_obj = GameInfo.objects.filter(game_id_id = g_id).order_by('id')
            html = []
            if not len(game_obj):
                raise ObjectDoesNotExist

            htm_format = '<tr><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td></tr>'

            html.append(htm_format %('game_id', 'White Move', 'Black Move', 'Check', 'Black King pos', 'White King pos'))
            for tuple in game_obj:
                html.append(htm_format %(tuple.game_id_id,tuple.white_move,tuple.black_move,tuple.check,tuple.black_king_pos,tuple.white_king_pos))

            html.append('Total db calls = %s'% str(len(connection.queries)))
            return HttpResponse('<table>%s</table>' % '\n'.join(html),status=200)

        else:
            return HttpResponse('Please use postman to input data or get using ?game_id= or ?player_id=', status = 200)
