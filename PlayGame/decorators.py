from django.core.exceptions import ObjectDoesNotExist
from django.utils.datastructures import MultiValueDictKeyError
from django.http import QueryDict
from django.db import IntegrityError, DataError
from django.http import HttpResponse
from .models import Player, Game, GameInfo

def check_exceptions(function):
    def wrapper(self, *args, **kwargs):
        try:
            return function(self, *args, **kwargs)
        except ObjectDoesNotExist:
            return HttpResponse('Error: Object does not exist',
            status = 500)
        except MultiValueDictKeyError:
            return HttpResponse('Error: MiltiValueDictKeyError',
            status = 500)
        except DataError:
            return HttpResponse('Data Error: Value too long',
            status = 500)
        except IntegrityError:
            return HttpResponse('Integrity Error: please resolve conflict',
            status = 500)

    return wrapper
