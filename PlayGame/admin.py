# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import Player, Game, GameInfo

# Register your models here.
class PlayerAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')

    def player_info(self, obj):
        return obj.description

    def get_queryset(self, request):
        queryset = super(PlayerAdmin, self).get_queryset(request)
        queryset = queryset.order_by('id')
        return queryset

    player_info.short_description = 'Player_Info'

class GameAdmin(admin.ModelAdmin):
    list_display = ('id', 'player_white', 'player_black', 'winner_id')

    def game_info(self, obj):
        return obj.description

    def get_queryset(self, request):
        queryset = super(GameAdmin, self).get_queryset(request)
        queryset = queryset.order_by('id')
        return queryset

    game_info.short_description = 'Game_Info'

class GameInfoAdmin(admin.ModelAdmin):
    list_display = ('game_id_id', 'white_move', 'black_move', 'check', 'white_pawn_count', 'black_pawn_count')

    def gameinfo_info(self, obj):
        return obj.description

    def get_queryset(self, request):
        queryset = super(GameInfoAdmin, self).get_queryset(request)
        queryset = queryset.order_by('id')
        return queryset

    gameinfo_info.short_description = 'GameInfo_Info'

admin.site.register(Player, PlayerAdmin)
admin.site.register(Game, GameAdmin)
admin.site.register(GameInfo, GameInfoAdmin)
