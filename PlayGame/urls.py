from django.conf.urls import url
from django.contrib import admin

from views import Register, Play, NewGame

urlpatterns = [
    url(r'^register/$', Register.as_view(), name = 'Register'),
    url(r'^newgame/$', NewGame.as_view(), name = 'New Game'),
    url(r'^play/', Play.as_view(), name = 'Play'),
]
