# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
class Player(models.Model):
    name = models.CharField(max_length = 25)

    def __unicode__(self):
        return str(self.id)

class Game(models.Model):
    id = models.AutoField(primary_key = True)
    player_white = models.ForeignKey(Player, on_delete = models.CASCADE,
    related_name = 'palyer_white')
    player_black = models.ForeignKey(Player, on_delete = models.CASCADE,
    related_name = 'player_black')
    winner = models.ForeignKey(Player, on_delete = models.CASCADE,
    related_name = 'winner', default = '')

class GameInfo(models.Model):
    # game_id = models.ForeignKey(Game, on_delete = models.CASCADE)
    game_id = models.ForeignKey(Game, on_delete = models.CASCADE,
    related_name = 'game_id')
    white_move = models.CharField(max_length = 20, default = 'none')
    black_move = models.CharField(max_length = 20, default = 'none')
    white_pawn_count = models.IntegerField(default = 15)
    black_pawn_count = models.IntegerField(default = 15)
    check = models.CharField(max_length = 5, default = 'no')
    white_king_pos = models.CharField(max_length = 10, default = 'e1')
    black_king_pos = models.CharField(max_length = 10, default = 'e8')

    # def __unicode__(self):
    #     return str(self.game_id)
