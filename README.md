# Assignment 2 - Slow Chess

### Introduction
This assignment stores the information of each chess match played and allows users to access their data to analyze their gameplay. The project is built on Postgresql and Django (v1.11.4).

### Prerequisites
Basic requirements:

* [Python 2.7](https://www.python.org/downloads/)
* [Postman](https://www.getpostman.com/)
* [Django](https://www.djangoproject.com/)
* [PostgreSQL](https://www.djangoproject.com/)

Also helpful:

* [PostgreSQL App](https://postgresapp.com/)

### Installation
- `pip install django==1.11.14`
- `pip install psycopg2`

##### MacOSX
- `brew install postgresql`

##### Linux (Debian)
- `sudo apt install postgresql`

### Running the code
- Change directory to the project folder.
- Run command `python manage.py runserver`
- To view data or modify using admin:
    - Open URL on browser `http://127.0.0.1:8000/admin`
    - username: `farzaan`
    - password: `ggwpggwp`
- To Add a new user:
    - Paste this URL in postman `http://127.0.0.1:8000/slowchess/register/`
    - Select **POST** and add values corresponding to the following keys:
        - `name` for username
- To Start a new game you must first select the players and the side they will player:
    - Paste this URL in postman `http://127.0.0.1:8000/slowchess/newgame/`
    - Select **POST** and add the username (**NOT User ID**) to the following keys:
        - `white` for username of player 1
        - `black` for username of player 2
        - **It is important to note the game id displayed**
- To Play the game:
    - Paste this URL in postman `http://127.0.0.1:8000/slowchess/play/`
    - Select **POST** and add values corresponding to the following keys:
        - `game_id` for the game's id provided when starting new game
        - `white_move` for the white player's move
        - `black_move` for the black player's move
    - How to play moves:
        - `e4` would represent the specified player has moved a pawn to **e4** block
        - `Kf3` would represent the respective player has moved the King to position **f3**.
        - `END` would represent the respective player either forfeit or was checkmate by the opponent.
        - `-` would be the input of the black player should the white player call the **END**
- To display database matches played by player hit `http://127.0.0.1:8000/slowchess/play/?player_id=7` where **7** is the id of that player
- To display the game information hit `http://127.0.0.1:8000/slowchess/play/?game_id=3` where **3** is the id of the game
